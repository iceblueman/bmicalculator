import React, {Component} from 'react'
import './BMI.css'

export default class BMI extends Component{

    constructor(props){
        super(props)
        this.state = {
            height: 170, // cms
            weight: 70 // kgs
        }
    }

    handleHeightChange(ev){
        this.setState({height : ev.target.value})
    }

    handleWeightChange(ev){
        this.setState({weight : ev.target.value})
    }

    displayHeight(){
        return `${this.state.height/100}`+` m`
    }

    displayWeight(){
        return `${this.state.weight}`+` kg`
    }

    displayBMI(){
        let bmi = Math.trunc(this.state.weight/((this.state.height/100)**2)*100);
        return bmi/100
    }

    displayClassification(){
        const bmi = parseFloat(this.displayBMI())
        return  bmi < 18.5 ? 'Underweight'
                : bmi < 25 ? 'Normal weight'
                : bmi < 30 ? 'Overweight'
                :'Obese';
    }

    render() {
        return <div className={"container"}>
            <h1>BMI Calculator</h1>
            <p>Height (in centimeters)</p>
            <p>
                <div className={"indicator"}>
                    <input type="range" min={150} max={220} value={this.state.height} onChange={(ev) => this.handleHeightChange(ev)}/>
                </div>
                <div className={"display-meassure"}>
                    {this.displayHeight()}
                </div>
            </p>
            <p>Weight (in kilograms)</p>
            <p>
                <div className={"indicator"}>
                    <input type="range" min={40} max={200} value={this.state.weight} onChange={(ev) => this.handleWeightChange(ev)}/>
                </div>
                <div className={"display-meassure"}>
                {this.displayWeight()}
            </div>
            </p>
            <p>
                Your BMI:
            </p>
            <div className={"result"}>
                {this.displayBMI()}
            </div>
                Your BMI suggests your are:
            <div className={"result"}>
                {this.displayClassification()}
            </div>

        </div>
    }
}